#!/usr/bin/bash

echo "max_parallel_downloads=10" | sudo tee -a /etc/dnf/dnf.conf
echo "fastestmirror=true" | sudo tee -a /etc/dnf/dnf.conf
sudo dnf update -y

# Enable RPM Fusion
sudo dnf -y install \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm \

sudo dnf install -y \
  neovim \
  timeshift \
  bat \
  exa \
  ranger \
  trash-cli \
  xclip \
  unrar \
  ffmpeg \
  dnf-automatic \
  htop \
  dkms \
  ripgrep \
  rclone \
  nitrogen \
  papirus-icon-theme \
  kitty \
  lxappearance \
  python3-i3ipc \
  bleachbit \
  vorta \
  ncdu \
  picom \
  fzf \
  sxiv \
  flameshot \
  polybar \
  xfce-polkit \
  w3m w3m-img \
  mpv \

#vscode
# sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
# sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
# dnf check-update
# sudo dnf install code -y

#brave
sudo dnf install dnf-plugins-core -y
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf install brave-browser -y

#librewolf
sudo rpm --import https://keys.openpgp.org/vks/v1/by-fingerprint/034F7776EF5E0C613D2F7934D29FBD5F93C0CFC3
sudo dnf config-manager --add-repo https://rpm.librewolf.net
sudo dnf install --refresh librewolf -y

#Multimedia codecs
sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel -y
sudo dnf install lame\* --exclude=lame-devel -y
sudo dnf group upgrade --with-optional Multimedia -y

# Autotiling script
mkdir -p /home/jan/.local/bin
curl https://raw.githubusercontent.com/nwg-piotr/autotiling/master/autotiling/main.py > /home/jan/.local/bin/autotiling
chmod +x /home/jan/.local/bin/autotiling

#Enable Flatpak repository
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#OnlyOffice
sudo flatpak install flathub org.onlyoffice.desktopeditors -y

# VLC
# flatpak install flathub org.videolan.VLC -y

# Use docker without sudo
# sudo groupadd docker
# sudo usermod -aG docker $USER

# Create a symlink to nvim
sudo ln -s $(which nvim) /usr/bin/vim

# Install AstroNvim
git clone https://github.com/AstroNvim/AstroNvim /home/jan/.config/nvim
sudo chown -R jan /home/jan/.config/nvim 

# Install rofi applets and themes
# mkdir /home/jan/.local/src/
# git clone --depth=1 https://github.com/adi1090x/rofi.git /home/jan/.local/src/
# chmod +x /home/jan/.local/src/rofi/setup.sh
# /home/jan/.local/src/rofi/setup.sh

# Install JetBrains Mono Nerd Font
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/JetBrainsMono.zip -P /home/jan/Pobrane
unzip /home/jan/Pobrane/JetBrainsMono.zip
rm /home/jan/Pobrane/JetBrainsMono.zip
mkdir -p /home/jan/.local/share/fonts/
mv /home/jan/Pobrane/* /home/jan/.local/share/fonts/
sudo fc-cache -fv

# Configure Papirus Folder colors
wget -qO- https://git.io/papirus-folders-install | sh
papirus-folders -C indigo

#Configure Git
git config --global user.name "Jan Majchrzak"
git config --global user.email "jan.majchrzak@protonmail.com"

#Generate RSA key pair
ssh-keygen -t rsa -f $HOME/.ssh/id_rsa -q -P ""

#Copy dotfiles from gitlab
function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}
cd ~
echo ".cfg" >> .gitignore
git clone --bare https://gitlab.com/TZdRnqqS8W82J384/dotfiles.git $HOME/.cfg
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} rm -rf {}
config checkout
config config --local status.showUntrackedFiles no

# Copy wallpapers from gitlab
git clone https://gitlab.com/persona36/wallpapers.git Obrazy/Tapety/

# Set wallpaper
nitrogen --set-auto Obrazy/Tapety/4.jpg 

# Copy wallpaper for system-wide use
sudo cp /home/jan/Obrazy/Tapety/4.jpg /usr/share/backgrounds/custom.jpg

# Customize lightdm appearance
sudo sed -i '/background/ s/default.png/custom.jpg/' /etc/lightdm/lightdm-gtk-greeter.conf
sudo sed -i 's/#icon-theme-name=/icon-theme-name=Papirus-Dark/' /etc/lightdm/lightdm-gtk-greeter.conf
sudo sed -i 's/#theme-name=/theme-name=Tokyonight-Storm-BL/' /etc/lightdm/lightdm-gtk-greeter.conf

#Enable automatic updates
sudo systemctl enable --now dnf-automatic-install.timer
sudo sed -i 's/apply_updates = no/apply_updates = yes/' /etc/dnf/automatic.conf
sudo sed -i 's/upgrade_type = default/upgrade_type = security/' /etc/dnf/automatic.conf

# Limit logs size
sudo sed -i 's/#SystemMaxUse=/SystemMaxUse=100M/' /etc/systemd/journald.conf

 # Disable waiting for network on boot
sudo systemctl disable NetworkManager-wait-online.service

# Set timezone
sudo timedatectl set-timezone Poland

echo "===================="
echo "post install script finished"
echo "Please remember to:"
echo "1. Download GTK theme from https://www.gnome-look.org/p/1681315 and put it into /usr/share/themes/"
echo "2. Edit sudoers file to grant sudo without password"
echo "3. Edit /etc/fstab to automount backup drive"
echo "4. Setup backups via Timeshift and Vorta"
echo "===================="

echo "Press any key to finish"
read
